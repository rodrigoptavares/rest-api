//
// ──────────────────────────────────────────────  ──────────
//   :::::: TODO ENDPOINT : :  :   :    :     :        :       // ────────────────────────────────────────────────────────
//


    //
    // ──────────────────────────────────────────────────────── I ──────────
    //   :::::: R E Q U I R E S : :  :   :    :     :        :          :
    // ──────────────────────────────────────────────────────────────────
    //

        const 
            service = require( './service.js' )
    //────────────────────────────────────────────────────────────────────────────────


    //
    // ────────────────────────────────────────────────────────────────── II ──────────
    //   :::::: A P I   C O M P O N E N T : :  :   :    :     :        :          :
    // ────────────────────────────────────────────────────────────────────────────
    //

    
        const endpoints = { 
            service: service
        }    

    //────────────────────────────────────────────────────────────────────────────────


    //
    // ────────────────────────────────────────────────────── III ──────────
    //   :::::: E X P O R T S : :  :   :    :     :        :          :
    // ────────────────────────────────────────────────────────────────
    //

    
        module.exports = endpoints

    //────────────────────────────────────────────────────────────────────────────────


//────────────────────────────────────────────────────────────────────────────────
